﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Group3FinalProject {
	 
	/// <summary>
	/// The ItemSet class represents an in-memory store of Items
	/// ItemSet also contains the business logic for the Items window
	/// </summary>
	public class ItemSet {
		
		#region Attributes

		/// <summary>
		/// List of Item objects
		/// </summary>
		public List<Item> myList { get; set; }

		/// <summary>
		/// An object of the DataAccess class to handle database read/write functions
		/// </summary>
		private DataAccess myDataAccess;

		/// <summary>
		/// Variable to store an updated ID when changing or adding an Item
		/// </summary>
		private int changedID;

		/// <summary>
		/// Variable to store an updated Name when changing or adding an Item
		/// </summary>
		private String changedName;

		/// <summary>
		/// Variable to store an updated Description when changing or adding an Item
		/// </summary>
		private String changedDesc;

		/// <summary>
		/// Variable to store an updated Price when changing or adding an item
		/// </summary>
		private Decimal changedCost;


		#endregion

		#region Properties
		/// <summary>
		/// List to hold Invoices matching the Item (used when seeing if item can be deleted)
		/// </summary>
		public List<Invoice> myInvoices;

		#endregion


		#region Methods

		/// <summary>
		/// Constructor for ItemSet class
		/// </summary>
		public ItemSet() {

			try {
				InitializeList(); //call method to pull items from database and populate myList
			
				//initialize variables for storing changes
				changedID = 0;
				changedName = "";
				changedDesc = "";
				changedCost = 0;
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}

			} //end constructor


		/// <summary>
		/// Creates the list of Item objects from data in the database
		/// </summary>
		public void InitializeList() {

			try {
				myList = new List<Item>(); // Initialize the myList object
				myDataAccess = new DataAccess(); //Initialize the myDataAccess object

				DataTable dt = new DataTable(); //Initialize data table to hold returned data
				int count = 0; //initialize count of number of rows returned

				//Get data from database and hold it temporarily in the data table
				dt = myDataAccess.ExecuteSQLStatement("SELECT ID, ItemName, Description, Cost FROM ITEMS ORDER BY ID ASC;", ref count);

				//Loop through the data and create Item objects
				Item tempItem;
				for (int i = 0; i < count; i++) {
					tempItem = new Item();
					tempItem.ItemID = Convert.ToInt32(dt.Rows[i].ItemArray[0]); //assign the Item ID
					tempItem.ItemName = dt.Rows[i].ItemArray[1].ToString(); //assign the Name
					tempItem.ItemDescription = dt.Rows[i].ItemArray[2].ToString(); //assign the Description
					tempItem.ItemCost = Convert.ToDecimal(dt.Rows[i].ItemArray[3]); //assign the Cost
					myList.Add(tempItem);
					} //end for
				} //end try

			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}


			} //end InitializeList


		/// <summary>
		/// Determines whether an attempted change will be allowed
		/// </summary>
		/// <param name="message">Reference to a Message field</param>
		/// <param name="ID">ID of the attempted change</param>
		/// <param name="name">Attempted name change</param>
		/// <param name="desc">Attempted description change</param>
		/// <param name="price">Attempted price change</param>
		/// <returns>True for acceptable change, false for not acceptable</returns>
		public bool ValidateChange(ref String message, String ID, String name, String desc, String price) {
			
			try {
				bool succeeded = decimal.TryParse(price, out changedCost); //try to parse a decimal
					if (succeeded) { //if the price was converted to a decimal
						changedID = Convert.ToInt32(ID); //save all of the changes locally
						changedName = name;
						changedDesc = desc;
						changedCost = Convert.ToDecimal(price);
						message = "Changes saved"; //return a message to the user
						return true;
						} //end if succeeded
					else {
						message = "That is not a valid price. Please enter price again."; //return a message to the user
						return false;
						} //end else
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end validateChange


		/// <summary>
		/// Committs a validated change to the database
		/// </summary>
		public void ItemChanged() {
			try {
				StringBuilder sb = new StringBuilder();
				sb.Append("UPDATE Items SET ItemName = ");
				sb.Append('\"');
				sb.Append(changedName);
				sb.Append('\"');
				sb.Append(",");
				sb.Append(" Description = ");
				sb.Append('\"');
				sb.Append(changedDesc);
				sb.Append('\"');
				sb.Append(",");
				sb.Append(" Cost = ");
				sb.Append(changedCost);
				sb.Append(" Where ID = ");
				sb.Append(changedID);
				sb.Append(";");

				String s = myDataAccess.ExecuteScalarSQL(sb.ToString());
				InitializeList(); //reset the List of Items to match the updated database

				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end ItemChanged


		/// <summary>
		/// Determines whether a given item is linked to Invoices
		/// </summary>
		/// <param name="ID">ID number of the item</param>
		/// <returns>True if the item is linked to invoices, False if the item is not linked to any invoices</returns>
		public bool HasInvoices(int ID) {

			try {
				myInvoices = new List<Invoice>(); // Initialize the list of Invoices

				DataTable dt2 = new DataTable(); //Initialize a data table to hold returned data
				int count = 0; //initialize count of number of rows returned


				StringBuilder sb = new StringBuilder(); //build a string for the query
				sb.Append("SELECT Invoices.InvoiceNum, Invoices.InvoiceDate, Invoices.TotalCharge FROM Invoices INNER JOIN InvoiceItems ON Invoices.InvoiceNum = InvoiceItems.InvoiceNum WHERE InvoiceItems.Item_ID =");
				// Jeff: This is no longer needed because I changed the Item_ID to a number field.
                //sb.Append('\''); //put a single quote in the query to distinguish a text field
				sb.Append(ID);
				//sb.Append('\''); //put a single quote in the query to end the text field
				sb.Append(";");

				//Get data from database and hold it temporarily in the data table
				dt2 = myDataAccess.ExecuteSQLStatement(sb.ToString(), ref count);

				if (count != 0) { //if there are linked Invoices
					//Loop through the data and create Invoice objects
					Invoice tempInvoice;
					for (int i = 0; i < count; i++) {
						tempInvoice = new Invoice();
						tempInvoice.InvoiceNum = Convert.ToInt32(dt2.Rows[i].ItemArray[0]); //assign the Invoice Number
						tempInvoice.InvoiceDate = (DateTime)dt2.Rows[i].ItemArray[1]; //assign the Item Name
						tempInvoice.TotalCharge = Convert.ToDecimal(dt2.Rows[i].ItemArray[2]); //assign the Total Charge
						myInvoices.Add(tempInvoice);
						} //end for
					return true;
					} //end if

				else { return false; } //if there are no linked invoices
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end HasInvoices


		/// <summary>
		/// Deletes a row from the database
		/// </summary>
		/// <param name="i">ID number of the row to delete</param>
		public void DeleteCurrentItem(int i) {
			try {
				StringBuilder sb = new StringBuilder();
				sb.Append("DELETE FROM ITEMS WHERE ID = "); //build the query in a string
				sb.Append(i);
				sb.Append(";");
				String s = myDataAccess.ExecuteScalarSQL(sb.ToString()); //send the query to the database
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			}//end deleteCurrentItem


		/// <summary>
		/// Gets the next available item ID number from the database
		/// </summary>
		/// <returns>Next available ID number</returns>
		public int GetNextID() {

			try {
				StringBuilder sb = new StringBuilder(); //build a string for the query
				sb.Append("SELECT ID FROM Items WHERE ID = ");
				sb.Append("("); //add an opening parenthesis to the query
				sb.Append("SELECT MAX");
				sb.Append("("); //add an opening parenthesis to the query
				sb.Append("ID");
				sb.Append(")"); //add a closing parenthesis
				sb.Append("FROM ITEMS");
				sb.Append(")"); //add a closing parenthesis
				sb.Append(";");
				String s = myDataAccess.ExecuteScalarSQL(sb.ToString()); //execute the statement to get the highest ID
				int i = Convert.ToInt32(s); //convert the returned ID to an int
				i++; //add 1
				return i;
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			}


		/// <summary>
		/// Commits a new row to the database
		/// </summary>
		public void SaveNewItem() {
			try {
				StringBuilder sb = new StringBuilder(); //create a string to hold the statement
				sb.Append("INSERT INTO ITEMS VALUES ");
				sb.Append("("); //add an opening parenthesis to the query
				sb.Append(changedID);
				sb.Append(",");
				sb.Append("\""); //add a double-quote to the query
				sb.Append(changedName);
				sb.Append("\""); //add a double-quote to the query
				sb.Append(",");
				sb.Append("\""); //add a double-quote to the query
				sb.Append(changedDesc);
				sb.Append("\""); //add a double-quote to the query
				sb.Append(",");
				sb.Append(changedCost);
				sb.Append(")"); //add a closing parenthesis to the query
				sb.Append(";");
				String s = myDataAccess.ExecuteScalarSQL(sb.ToString()); //run the statement
				InitializeList(); //build the List again using the updated database table
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			}
		#endregion

		} //end class
	} //end namespace
