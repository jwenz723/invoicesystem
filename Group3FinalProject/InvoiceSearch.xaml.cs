﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;

namespace Group3FinalProject
{

    /// <summary>
    /// Interaction logic for InvoiceSearch.xaml
    /// </summary>
    public partial class InvoiceSearch : Window
    {

        #region Attributes
        /// <summary>
        /// This is the invoice that the user selects to view on the main form.  All invoice properties 
        /// should be stored into this object before the window is hidden if the user presses the 
        /// "select invoice" button.
        /// </summary>
        public Invoice SelectedInvoice { get; set; }

        /// <summary>
        /// Instance of the class that holds the invoices in the system
        /// </summary>
        public InvoiceSet myInvoiceSet { get; set; }

        /// <summary>
        /// Indicates if the user selected an invoice to view or not. This needs to be set to 
        /// "true" if the user clicked the "select invoice "button.
        /// </summary>
        public bool InvoiceSelected { get; set; }

        /// <summary>
        /// Flag that disables running filters on the datagrid when the user wants to reset their filter selection
        /// </summary>
        private bool resetingFilters = false;

        /// <summary>
        /// Worker that asynchronously loads the MyInvoiceSet property from the database
        /// </summary>
        private BackgroundWorker LoadInvoicesWorker;
        #endregion
        #region Constructor
        /// <summary>
        /// Constructor for the InvoiceSearch window class
        /// </summary>
        public InvoiceSearch()
        {
            try
            {
                InitializeComponent();

                // create the invoices worker and tell it what to do when it runs and what to run when it finishes
                LoadInvoicesWorker = new BackgroundWorker();
                LoadInvoicesWorker.DoWork += new DoWorkEventHandler(LoadInvoicesWorker_DoWork);
                LoadInvoicesWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoadInvoicesWorker_RunWorkerCompleted);
                // fill the data grid with invoices
                LoadInvoicesWorker.RunWorkerAsync();


            } //end try
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            } //end catch
        }
        #endregion Constructor
        #region Methods
        /// <summary>
        /// Handles an error
        /// </summary>
        /// <param name="sClass">The class in which the error occurred</param>
        /// <param name="sMethod">The method in which the error occurred</param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            } //end try

            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }
        
        /// <summary>
        /// Fills the search comboboxes with the data found in the invoices
        /// </summary>
        private void RefreshComboboxes()
        {
            // clear out anything in there already
            cbSrchInvoiceNumber.Items.Clear();
            cbSrchInvoiceDate.Items.Clear();
            cbSrchTotalCharge.Items.Clear();
            //
            // look at each invoice
            foreach (Invoice tempInvoice in myInvoiceSet.Invoices)
            {
                // add the invoice number, invoice date, and total charge to the appropriate combobox
                cbSrchInvoiceNumber.Items.Add(tempInvoice.InvoiceNum);

                // check if the passed in date is already in the combobox
                if (!cbSrchInvoiceDate.Items.Contains(tempInvoice.InvoiceDate))
                {
                    cbSrchInvoiceDate.Items.Add(tempInvoice.InvoiceDate);
                }

                // format the total charge as currency with a dollar sign
                string charge = string.Format("{0:C}", tempInvoice.TotalCharge);

                //don't add the charge if it's already in the list
                if(!cbSrchTotalCharge.Items.Contains(charge))
                {
                    cbSrchTotalCharge.Items.Add(charge);
                }
            }

        }

        /// <summary>
        /// Updates the UI to match any changes in the class attribute MyInvoiceSet
        /// </summary>
        public void RefreshInvoiceUI()
        {
            try
            {
                // Sync the invoice set with the database
                myInvoiceSet = new InvoiceSet();

                // Refresh the filter comboboxes
                RefreshComboboxes();

                // Jeff: This updates the items contained in the dataset because it sets the itemsource of the datagrid
                ResetFilters();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            } //end catch
        }

        /// <summary>
        /// Resets the filters (and thus the DataGrid) back to their default state
        /// </summary>
        private void ResetFilters()
        {
            // mark this reseting filters flag so that we don't process the filter logic
            resetingFilters = true;

            // reset all three comboboxes to not show a selection
            cbSrchInvoiceNumber.SelectedIndex = -1;
            cbSrchInvoiceDate.SelectedIndex = -1;
            cbSrchTotalCharge.SelectedIndex = -1;

            // reset the flag since we're done changing indexes
            resetingFilters = false;

            // reset data grid to show all invoices
            dataGridInvoiceSearch.ItemsSource = myInvoiceSet.Invoices;
        }
        #endregion Methods
        #region UI Event Handlers
        /// <summary>
        /// Handle things to do when the datagrid selection is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridInvoiceSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // check if there's an item selected (-1 is nothing selected)
            if (dataGridInvoiceSearch.SelectedIndex > -1)
            {
                // an invoice is selected, enable the select button
                btnSelectInvoice.IsEnabled = true;
            }
            else
            {
                // an invoice is not selected, disable the select button
                btnSelectInvoice.IsEnabled = false;
            }
        }
        /// <summary>
        /// Handles user clicking the Close button
        /// </summary>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //reset filters to default so they're already empty on the next load
                ResetFilters();
                this.Hide();
            } //end try
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Resets the filter comboboxes and resets the datagrid to show all invoices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnResetFilters_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResetFilters();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Stores the selected invoice into the object "selectedInvoice" and sets the property "invoiceSelected" to true
        /// then hides the window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //ignore the click if the user hasn't made a selection
                if (dataGridInvoiceSearch.SelectedItem == null) { return; }

                // put the currently selected invoice in the datagrid into the local invoice object
                SelectedInvoice = (Invoice)dataGridInvoiceSearch.SelectedItem;

                // update to true so that the main form knows that an invoice was selected
                InvoiceSelected = true;

                //Reset the filters back to default
                ResetFilters();
                this.Hide();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        
        /// <summary>
        /// Filters the datagrid based on the current selection of each of the comboboxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbSrch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // if we're currently reseting the filters, ignore the event
                // this gets fired when i reset the selection back to -1 (nothing selected) and I don't want to filter the datagrid at that time
                if (resetingFilters) { return; }

                // create a list of the Invoices we want to apply a filter to
                List<Invoice> invoicesToFilter;
                // create a list of Invoices that have had a filter applied to them
                List<Invoice> filteredInvoices = new List<Invoice>();

                // we're starting out, so no previous filter has run
                bool PreviousFilterHasRun = false;

                // filter the invoice numbers; anything above -1 means something was selected
                if (cbSrchInvoiceNumber.SelectedIndex > -1)
                {
                    // check if a filter has already run against the invoices
                    if (PreviousFilterHasRun)
                    {
                        // a filter has already run, filter from the filtered list
                        invoicesToFilter = new List<Invoice>(filteredInvoices);
                    }
                    else
                    {
                        // a filter has not run yet, filter from the full list
                        invoicesToFilter = new List<Invoice>(myInvoiceSet.Invoices);
                    }
                    // mark the previous filter flag, as we are now filtering the data
                    PreviousFilterHasRun = true;

                    // run linq against our invoices, select those that match the selection in the combobox
                    IEnumerable<Invoice> invoiceFilter =
                        from Invoice invoiceToCheck in invoicesToFilter
                        where invoiceToCheck.InvoiceNum == Int32.Parse(cbSrchInvoiceNumber.SelectedItem.ToString())
                        select invoiceToCheck;

                    // clear out invoices that previously matched, since our new query will replace them
                    filteredInvoices.Clear();

                    // add the invoices that matched the filter to the filtered list
                    foreach (Invoice filtered in invoiceFilter)
                    {
                        filteredInvoices.Add(filtered);
                    }
                }

                // filter out by the date selection; anything above -1 means something was selected
                if (cbSrchInvoiceDate.SelectedIndex > -1)
                {
                    // check if a filter has already run against the invoices
                    if (PreviousFilterHasRun)
                    {
                        // a filter has already run, filter from the filtered list
                        invoicesToFilter = new List<Invoice>(filteredInvoices);
                    }
                    else
                    {
                        // a filter has not run yet, filter from the full list
                        invoicesToFilter = new List<Invoice>(myInvoiceSet.Invoices);
                    }
                    // mark the previous filter flag, as we are now filtering the data
                    PreviousFilterHasRun = true;

                    // run linq against our invoices, select those that match the selection in the combobox
                    IEnumerable<Invoice> invoiceFilter =
                        from Invoice invoiceToCheck in invoicesToFilter
                        where invoiceToCheck.InvoiceDate == Convert.ToDateTime(cbSrchInvoiceDate.SelectedItem.ToString())
                        select invoiceToCheck;

                    // clear out invoices that previously matched, since our new query will replace them
                    filteredInvoices.Clear();

                    // add the invoices that matched the filter to the filtered list
                    foreach (Invoice filtered in invoiceFilter)
                    {
                        filteredInvoices.Add(filtered);
                    }
                }
                // filter by the total charge; anything above -1 means something was selected
                if (cbSrchTotalCharge.SelectedIndex > -1)
                {
                    // check if a filter has already run against the invoices
                    if (PreviousFilterHasRun)
                    {
                        // a filter has already run, filter from the filtered list
                        invoicesToFilter = new List<Invoice>(filteredInvoices);
                    }
                    else
                    {
                        // a filter has not run yet, filter from the full list
                        invoicesToFilter = new List<Invoice>(myInvoiceSet.Invoices);
                    }
                    // mark the previous filter flag, as we are now filtering the data
                    PreviousFilterHasRun = true;

                    // clear out invoices that previously matched, since our new query will replace them
                    IEnumerable<Invoice> invoiceFilter =
                        from Invoice invoiceToCheck in invoicesToFilter
                        where invoiceToCheck.TotalCharge == Decimal.Parse(cbSrchTotalCharge.SelectedItem.ToString(), NumberStyles.Currency)
                        select invoiceToCheck;

                    // clear out invoices that previously matched, since our new query will replace them
                    filteredInvoices.Clear();

                    // add the invoices that matched the filter to the filtered list
                    foreach (Invoice filtered in invoiceFilter)
                    {
                        filteredInvoices.Add(filtered);
                    }
                }

                // the filteredInvoices list has all the Invoices that matched the user's criteria in it, bind that to the datagrid to show the user the results
                dataGridInvoiceSearch.ItemsSource = filteredInvoices;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            } //end catch
        }

        /// <summary>
        /// Handles user clicking the X button
        /// </summary>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                // cancel the event so that the instance of this class doesn't get destroyed
                e.Cancel = true;
                ResetFilters();
                this.Hide();
            } //end try
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        #endregion UI Event Handlers
        #region BackgroundWorker Event Handlers
        /// <summary>
        /// Creates an InvoiceSet object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadInvoicesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //NOTE: This is done asychronously because creating an InvoiceSet involves querying the database
            //Create the InvoiceSet and return it as the result
            e.Result = new InvoiceSet();
        }

        /// <summary>
        /// Loads the InvoiceSet into the UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Pass in an InvoiceSet</param>
        private void LoadInvoicesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                // cast and set our InvoiceSet to the InvoiceSet made by the worker
                myInvoiceSet = (InvoiceSet)e.Result;
                // bind the invoice list to the datagrid
                dataGridInvoiceSearch.ItemsSource = myInvoiceSet.Invoices;
                // select the first index of the datagrid
                dataGridInvoiceSearch.SelectedIndex = 0;
                // fill the comboboxes with filter choices, from the invoices
                RefreshComboboxes();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            } //end catch
        }
        #endregion BackgroundWorker Event Handlers
    } //end class
} //end namespace