﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Group3FinalProject {

	/// <summary>
	/// Class for access to the database
	/// </summary>
	public class DataAccess {

		#region Attributes
		/// <summary>
		/// Connection string to the database.
		/// </summary>
		private string myConnectionString;
		#endregion

		#region Methods

		/// <summary>
		/// Constructor for object class
		/// </summary>
		public DataAccess() {
			try {
				myConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data source= " + 
					Directory.GetCurrentDirectory() + "\\Group3StoreDatabase.mdb";
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end constructor

		/// <summary>
		/// Executes the desired SQL statement against this class's database connection
		/// </summary>
		/// <param name="sSQL">SQL string to execute</param>
		/// <param name="iRetVal">reference to number of values returned</param>
		/// <returns></returns>
		public DataTable ExecuteSQLStatement(string sSQL, ref int iRetVal) {
			try {
			//Create a new DataTable
			DataTable dt = new DataTable();

			using (OleDbConnection conn = new OleDbConnection(myConnectionString)) {
				using (OleDbDataAdapter adapter = new OleDbDataAdapter()) {

					//Open the connection to the database
					conn.Open();

					//Add the information for the SelectCommand using the SQL statement and the connection object
					adapter.SelectCommand = new OleDbCommand(sSQL, conn);
					adapter.SelectCommand.CommandTimeout = 0;

					//Fill up the DataTable with data
					adapter.Fill(dt);
					} //end using
				} //end using

				//Set the number of values returned
				iRetVal = dt.Rows.Count;

				//return the DataTable
				return dt;
				}
			catch (Exception ex) {
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + 
					MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end ExecuteSQLStatement

		/// <summary>
		/// This method takes an SQL statment that is passed in and executes it.  The resulting single 
		/// value is returned.
		/// </summary>
		/// <param name="sSQL">The SQL statement to be executed.</param>
		/// <returns>Returns a string from the scalar SQL statement.</returns>
		public string ExecuteScalarSQL(string sSQL) {
			try {
				//Holds the return value
				object obj = null;

				using (OleDbConnection conn = new OleDbConnection(myConnectionString)) {
					using (OleDbDataAdapter adapter = new OleDbDataAdapter()) {

						//Open the connection to the database
						conn.Open();

						//Add the information for the SelectCommand using the SQL statement and the connection object
						adapter.SelectCommand = new OleDbCommand(sSQL, conn);
						adapter.SelectCommand.CommandTimeout = 0;

						//Execute the scalar SQL statement
						obj = adapter.SelectCommand.ExecuteScalar();
						}
					}

				//See if the object is null
				if (obj == null) {
					//Return a blank
					return "";
					}
				else {
					//Return the value
					return obj.ToString();
					}
				}
			catch (Exception ex) {
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
					MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			}//end ExecuteScalarSQL

        /// <summary>
        /// This method takes an SQL statment that is a non query and executes it.
        /// </summary>
        /// <param name="sSQL">The SQL statement to be executed.</param>
        /// <returns>Returns the number of rows affected by the SQL statement.</returns>
        public int ExecuteNonQuery(string sSQL)
        {
            try
            {
                //Number of rows affected
                int iNumRows;

                using (OleDbConnection conn = new OleDbConnection(myConnectionString))
                {
                    //Open the connection to the database
                    conn.Open();

                    //Add the information for the SelectCommand using the SQL statement and the connection object
                    OleDbCommand cmd = new OleDbCommand(sSQL, conn);
                    cmd.CommandTimeout = 0;

                    //Execute the non query SQL statement
                    iNumRows = cmd.ExecuteNonQuery();
                }

                //return the number of rows affected
                return iNumRows;
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

		#endregion

			} //end class
		} //end namespace
