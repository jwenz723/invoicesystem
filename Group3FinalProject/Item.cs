﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group3FinalProject
{

    /// <summary>
    /// Object representing a single inventory item
    /// </summary>
    public class Item
    {

        #region Properties
        /// <summary>
        /// The ID number of the item
        /// </summary>
        public int ItemID { get; set; }

        /// <summary>
        /// A one-word item name
        /// </summary>
        public String ItemName { get; set; }

        /// <summary>
        /// A description of the item
        /// </summary>
        public String ItemDescription { get; set; }

        /// <summary>
        /// The cost of the item in 0.00 currency format
        /// </summary>
        public decimal ItemCost { get; set; }

        /// <summary>
        /// Used as part of the primary key in the InvoiceItems database table
        /// </summary>
        public int InvoiceRowNum { get; set; }

        #endregion

        /// <summary>
        /// Overrides the ToString method to provide a tab-separated text string representing the object
        /// </summary>
        /// <returns></returns>
        override public String ToString()
        {
            //StringBuilder sb = new StringBuilder();
            //sb.Append(ItemID.ToString());
            //sb.Append('\t');
            //sb.Append(ItemName);
            //sb.Append('\t');
            //sb.Append(ItemDescription);
            //sb.Append('\t');
            //sb.Append(ItemCost.ToString());
            //return sb.ToString();
            return ItemID + " " + ItemName;
        } //end ToString


    } //end class
} //end namespace
