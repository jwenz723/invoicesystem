﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Group3FinalProject {

	/// <summary>
	/// Interaction logic for NewItem.xaml
	/// The New Item window appears when the user clicks the "New" button on the Items window
	/// </summary>
	public partial class NewItem : Window {

		#region Attributes
		/// <summary>
		/// Object of the ItemSet class, which provides methods required for this form's validation
		/// </summary>
		ItemSet myItemSet = new ItemSet();
		#endregion

		#region Properties
		/// <summary>
		/// itemsRef is a reference to the Items form from which this form was created
		/// this reference allows this form to call the methods of the Items class
		/// </summary>
		public Items itemsRef {get; set; }
		#endregion


		#region Methods
		/// <summary>
		/// Constructor
		/// </summary>
		public NewItem() {
			try {
				InitializeComponent();

				//find the next available ID number from the database and set it as the ID of the new record (not editable)
				tbNewID.Text = myItemSet.GetNextID().ToString(); 
				} //end try

			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				} //end catch
															
			} //end constructor


		/// <summary>
		/// Event handler for the Save button
		/// </summary>
		private void btnNewSave_Click(object sender, RoutedEventArgs e) {
			try {
				String message = ""; //variable to store a message that might need to be displayed back to the user

				if (myItemSet.ValidateChange(ref message, tbNewID.Text,  //see if the change is valid
						tbNewName.Text, tbNewDesc.Text, tbNewPrice.Text) == true) {

						myItemSet.SaveNewItem(); //add the new item
						itemsRef.loadGrid(); //call the Items window to reload the grid to show the new item
						this.Hide(); //hide this form and return to the Items form
					} //end if

				else {
					lblMessage.Content = message; //set the status message to the user
					} //end else
				} //end try

			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			} //end btnSaveClick
		

		/// <summary>
		/// Event handler for the Cancel button
		/// </summary>
		private void btnNewCancel_Click(object sender, RoutedEventArgs e) {
			try {
				this.Hide(); //hide this form and return to the Items form
				} //end try

			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			} //end btnNewCancel_click

		/// <summary>
		/// Handles an error
		/// </summary>
		/// <param name="sClass">The class in which the error occurred</param>
		/// <param name="sMethod">The method in which the error occurred</param>
		private void HandleError(string sClass, string sMethod, string sMessage) {
			try {
				MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
				} //end try

			catch (Exception ex) {
				System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
											 "HandleError Exception: " + ex.Message);
				}
			}
		#endregion


		} //end class
	} //end namespace
