﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;
using System.Collections.ObjectModel;

namespace Group3FinalProject
{
    /// <summary>
    /// Holds an in-memory representation of the invoices in the database
    /// Also handles business logic for the Invoices window
    /// </summary>
    public class InvoiceSet
    {
        public List<Invoice> Invoices { get; set; }
        private DataAccess MyDataAccess { get; set; }

        /// <summary>
        /// Creates an Invoice set with the Invoices property filled with a list of all invoices in the database
        /// </summary>
        public InvoiceSet()
        {
            try
            {
                //instantiate the data access object
                MyDataAccess = new DataAccess();
                //instantiate the invoice list
                Invoices = new List<Invoice>();
                // rehydrate the invoices from the database
                RehydrateInvoices();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            } //end catch
        }

        /// <summary>
        /// Rehydrates invoices from the database and stores them in the Invoices property
        /// </summary>
        private void RehydrateInvoices()
        {
            try
            {
                DataTable invoiceRows;
                int numRows = 0;
                // get all invoices from the database
                string getInvoices = "SELECT InvoiceNum, InvoiceDate, TotalCharge FROM Invoices";
                invoiceRows = MyDataAccess.ExecuteSQLStatement(getInvoices, ref numRows);

                // for each row returned, create an Invoice object and set the properties of the invoice, then add it to the Invoices list
                for (int i = 0; i < numRows; i++)
                {
                    Invoice tempInvoice = new Invoice();
                    tempInvoice.InvoiceNum = Int32.Parse(invoiceRows.Rows[i][0].ToString());
                    tempInvoice.InvoiceDate = Convert.ToDateTime(invoiceRows.Rows[i][1]);
                    tempInvoice.TotalCharge = Convert.ToDecimal(invoiceRows.Rows[i][2]);
                    Invoices.Add(tempInvoice);
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            } //end catch
        }
    }
}
