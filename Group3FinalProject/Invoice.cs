﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Group3FinalProject
{

    /// <summary>
    /// Object representing a single Invoice
    /// </summary>
    public class Invoice
    {

        #region Properties
        /// <summary>
        /// The ID number of the item
        /// </summary>
        public int InvoiceNum { get; set; }

        /// <summary>
        /// The invoice date
        /// </summary>
        public DateTime InvoiceDate { get; set; }

        /// <summary>
        /// The invoice total charge
        /// </summary>
        public decimal TotalCharge { get; set; }

        /// <summary>
        /// Contains all items that are within an invoice
        /// </summary>
        public List<Item> myItems { get; set; }

        /// <summary>
        /// Stores a table after a sql query is made
        /// </summary>
        private DataTable dt;

        /// <summary>
        /// Used to connect to the database
        /// </summary>
        private DataAccess da;

        /// <summary>
        /// Holds a sql string used to send as a parameter into the DataAccess class
        /// </summary>
        private string sSQL;

        /// <summary>
        /// Holds return data from sql queries
        /// </summary>
        private int iRet;

        /// <summary>
        /// Holds return data from scalar sql queries
        /// </summary>
        private string sRet;

        #endregion

        #region Constructor
        public Invoice()
        {
            clearInvoice();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Resets all the properties of the Invoice back to their default value
        /// </summary>
        private void clearInvoice()
        {
            // Clear/Instantiate the values
            InvoiceNum = -1;
            InvoiceDate = new DateTime();
            TotalCharge = -1;
            myItems = new List<Item>();
        }

        /// <summary>
        /// Overrides the ToString method to provide a comma-separated set of Invoice values
        /// </summary>
        /// <returns>String representing the Invoice object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(InvoiceNum.ToString());
            sb.Append(", ");
            sb.Append(InvoiceDate.ToString());
            sb.Append(", ");
            sb.Append(TotalCharge.ToString());
            return sb.ToString();
        }

        #endregion

        #region SQL Methods

        /// <summary>
        /// Queries the database to get all of the items contained in the invoice. 
        /// </summary>
        /// <returns>A table of all items contained in the invoice. Columns: ID, ItemName, Description, Cost</returns>
        public void getInvoiceItems()
        {
            try
            {
                // Instantiate the objects for accessing the database
                sSQL = "SELECT InvoiceRowNum, ID, ItemName, Description, Cost FROM InvoiceItems LEFT JOIN Items ON InvoiceItems.item_ID = items.ID WHERE InvoiceNum =" + InvoiceNum.ToString();
                iRet = 0;
                dt = new DataTable();
                da = new DataAccess();

                dt = da.ExecuteSQLStatement(sSQL, ref iRet);

                // Get the TotalCharge for the invoice
                updateTotalCharge(dt, iRet);

                myItems = new List<Item>();
                Item invoiceItem;

                // Loop through all the rows of items that are contained in the database that match this invoice
                foreach (DataRow myRow in dt.Rows)
                {
                    // Declare a new Item
                    invoiceItem = new Item();
                    
                    // Add all the data pertaining to that one invoice item
                    invoiceItem.InvoiceRowNum = Convert.ToInt32(myRow["InvoiceRowNum"]);
                    invoiceItem.ItemID = Convert.ToInt32(myRow["ID"]);
                    invoiceItem.ItemName = myRow["ItemName"].ToString();
                    invoiceItem.ItemDescription = myRow["Description"].ToString();
                    invoiceItem.ItemCost = Convert.ToDecimal(myRow["Cost"]);

                    // Add the item into the list
                    myItems.Add(invoiceItem);
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Updates the total price of an invoice in the database.  This function is not specific to any invoice.  It updates the invoice
        /// corresponding to the invoice number that is passed in as a parameter.
        /// </summary>
        /// <param name="inputPrice">Price to set</param>
        /// <param name="inputInvoiceNum">Invoice to modify</param>
        /// <returns>True if successful update</returns>
        public static bool updateTotalCharge(string inputPrice, string inputInvoiceNum)
        {
            try
            {
                // Instantiate the database access class
                DataAccess da = new DataAccess();

                // Create the sql statement to update the total charge of an invoice
                string sSQL = "Update Invoices Set TotalCharge = " + inputPrice + " Where InvoiceNum = " + inputInvoiceNum;

                // run the update query
                int iRet = da.ExecuteNonQuery(sSQL);

                // If successful insert
                if (iRet == 1)
                {
                    return true;
                }

                // Something failed or there is no items in the invoice
                return false;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Overloaded version so that we don't have to query the database for the DataTable if we have already
        /// received it before the function is called.
        /// 
        /// Refreshes the TotalCharge property of the invoice after pulling new data from the database
        /// </summary>
        /// <param name="dt">Table of invoice data that is being passed in</param>
        /// <param name="iRet">Amount of items in the invoice</param>
        /// <returns>True if successful update of total charge in the database and in the class level property.  If there are no items in the invoice, false will be returned.</returns>
        public bool updateTotalCharge(DataTable dt, int iRet)
        {
            try
            {
                // Reset the TotalCharge property of the invoice back to 0
                TotalCharge = 0;

                // If there are rows/items in the invoice
                if (iRet != 0)
                {
                    // Loop through each row that was returned from the database and sum up the charge of each item in the invoice
                    foreach (DataRow myRow in dt.Rows)
                    {
                        TotalCharge += Convert.ToDecimal(myRow["Cost"]);
                    }
                }

                // Instantiate the database access class
                da = new DataAccess();

                // Create the sql statement to update the TotalCharge in the invoice in the database table "Invoices"
                sSQL = "Update Invoices Set TotalCharge = " + TotalCharge.ToString() + " Where InvoiceNum = " + InvoiceNum.ToString();

                // run the update query
                iRet = da.ExecuteNonQuery(sSQL);

                // If successful update
                if (iRet == 1)
                {
                    return true;
                }

                // Something failed or there is no items in the invoice
                return false;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Creates a new invoice in the database
        /// </summary>
        /// <returns>The invoice number</returns>
        public int createInvoice(string stringDate)
        {
            try
            {
                DateTime newInvoiceDate = new DateTime();

                // If the user selected a date
                if (stringDate != "") {
                    newInvoiceDate = Convert.ToDateTime(stringDate);
                }
                // If no date was selected by the user then set the invoice date to the current date
                else
                {
                    newInvoiceDate = DateTime.Today;
                }

                // Instantiate the database access class
                da = new DataAccess();

                // Create the sql statement to create the invoice in the database (invoice number will be autogenerated
                sSQL = "INSERT INTO Invoices (InvoiceDate) VALUES (#" + newInvoiceDate.Year + "-" + newInvoiceDate.Month + "-" + newInvoiceDate.Day + "#)";

                // run the create query
                iRet = da.ExecuteNonQuery(sSQL);

                // If successful insert
                if (iRet == 1)
                {
                    // Clear the dataTable
                    dt = new DataTable();

                    // Create a sql query to get our invoiceNum
                    sSQL = "SELECT MAX(InvoiceNum) As MyInvoiceNum FROM Invoices";

                    // run the query to get the invoiceNum of the invoice that was just created (will have the biggest invoiceNum)
                    dt = da.ExecuteSQLStatement(sSQL, ref iRet);

                    // If successful fetch of invoiceNum
                    if (iRet == 1)
                    {
                        // Store the new invoice number in the invoice
                        InvoiceNum = Convert.ToInt32(dt.Rows[0]["MyInvoiceNum"]);

                        // Return the invoice number of the new invoice
                        return InvoiceNum;
                    }
                }

                throw new Exception("Unable to create entry in the database");
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Deletes the invoice from the database
        /// </summary>
        /// <returns>True if a successful delete occured.</returns>
        public bool deleteInvoice()
        {
            try
            {
                // Instantiate the database access class
                da = new DataAccess();

                // Create the sql statement to delete the invoice from the database
                sSQL = "Delete FROM Invoices WHERE InvoiceNum =" + InvoiceNum;

                // run the delete query
                iRet = da.ExecuteNonQuery(sSQL);

                // If successful delete
                if (iRet == 1)
                {
                    // Create the sql statement to delete the invoice from InvoiceItems table in the database
                    sSQL = "Delete FROM InvoiceItems WHERE InvoiceNum =" + InvoiceNum;

                    // run the delete query
                    iRet = da.ExecuteNonQuery(sSQL);

                    return true;
                }

                // The delete did not occur
                return false;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Adds an item into the database and myList (TODO: break into a background worker the SQL)
        /// </summary>
        /// <param name="itemToAdd">Item to be added</param>
        public void addItem(Item newItem)
        {
            try
            {
                // Store a copy of the item that was passed in
                Item itemToAdd = new Item();
                itemToAdd.ItemCost = newItem.ItemCost;
                itemToAdd.ItemDescription = newItem.ItemDescription;
                itemToAdd.ItemID = newItem.ItemID;
                itemToAdd.ItemName = newItem.ItemName;

                // Set a base invoiceRowNum to be used for the insert of the new item into the database
                int invoiceRowNum = 1;

                // Loop through all items that are contained in the invoice and set invoiceRowNum
                // to be 1 greater than the current highest InvoiceRowNum
                foreach (Item testItem in myItems)
                {
                    if (invoiceRowNum <= testItem.InvoiceRowNum)
                    {
                        invoiceRowNum = testItem.InvoiceRowNum + 1;
                    }
                }

                itemToAdd.InvoiceRowNum = invoiceRowNum;

                // Store the items necessary for the backgroundworker to make the database addition
                string[] backgroundWorkerData = new string[3];
                backgroundWorkerData[0] = invoiceRowNum.ToString();
                backgroundWorkerData[1] = InvoiceNum.ToString();
                backgroundWorkerData[2] = itemToAdd.ItemID.ToString();

                // Handle the actual addition of the item in the database using a background worker so the UI is not slowed
                // down by the SQL query
                BackgroundWorker addItemWorker = new BackgroundWorker();
                addItemWorker.DoWork += new DoWorkEventHandler(addItemWorker_DoWork);
                addItemWorker.RunWorkerAsync(backgroundWorkerData);
                
                // If the invoice doesn't contain any items yet
                if (TotalCharge == -1)
                {
                    TotalCharge = itemToAdd.ItemCost;
                }
                else
                {
                    TotalCharge += itemToAdd.ItemCost;
                }

                //Add the Item into the list object
                myItems.Add(itemToAdd);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Deletes an item from the database and myList (TODO: break into a background worker the SQL)
        /// </summary>
        /// <param name="itemToDelete">The item to be deleted from the invoice</param>
        public void deleteItem(Item itemToDelete)
        {
            try
            {
                // Store the items necessary for the backgroundworker to make the database deletion
                string[] backgroundWorkerData = new string[2];
                backgroundWorkerData[0] = itemToDelete.InvoiceRowNum.ToString();
                backgroundWorkerData[1] = InvoiceNum.ToString();

                // Handle the actual delete of the item in the database using a background worker so the UI is not slowed
                // down by the SQL query
                BackgroundWorker deleteItemWorker = new BackgroundWorker();
                deleteItemWorker.DoWork += new DoWorkEventHandler(deleteItemWorker_DoWork);
                deleteItemWorker.RunWorkerAsync(backgroundWorkerData);

                // Update the TotalCharge Property
                TotalCharge -= itemToDelete.ItemCost;

                // Remove the item from the list of items contained within the invoice object
                myItems.Remove(itemToDelete);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

        #region BackgroundWorker Methods

        /// <summary>
        /// Runs a sql query to delete an item from an invoice in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteItemWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // Get the data pertaining to the item to add into the database from the array that was passed in.
                // order of elements is invoiceRowNum, InvoiceNum
                string[] myData = (string[])e.Argument;

                // Store the info used for the sql query
                string deleteInvoiceRowNum = myData[0];
                string deleteInvoiceNum = myData[1];

                // Instantiate the database access class
                da = new DataAccess();

                sSQL = "DELETE FROM InvoiceItems WHERE InvoiceNum = " + deleteInvoiceNum + " And InvoiceRowNum = " + deleteInvoiceRowNum;

                // run the delete query
                iRet = da.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Runs a sql query to add an item to an invoice in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addItemWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // Get the data pertaining to the item to add into the database from the array that was passed in.
                // order of elements is invoiceRowNum, InvoiceNum, item_ID
                string[] myData = (string[])e.Argument;

                // Instantiate the database access class
                da = new DataAccess();

                sSQL = "INSERT INTO InvoiceItems (InvoiceRowNum,InvoiceNum,Item_ID) VALUES (" + myData[0] + "," + myData[1] + "," + myData[2] + ")";


                // run the delete query
                iRet = da.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

    } //end class
} //end namespace
