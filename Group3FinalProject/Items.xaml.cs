﻿

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.Windows.Shapes;

namespace Group3FinalProject {

	/// <summary>
	/// Interaction logic for Items.xaml
	/// </summary> 
	public partial class Items : Window {

		#region Attributes

		/// <summary>
		/// ItemSet class contains cached data and business logic for the Items window
		/// </summary>
		ItemSet myItemSet;

		/// <summary>
		/// variable representing the row currently highlighted by the user
		/// used to return highlight to the same row after deleting and recreating the data grid
		/// </summary>
		int selectedRow;

		/// <summary>
		/// Window to allow a user to add a new item
		/// </summary>
		NewItem myNewItem;

		#endregion


		#region Methods
		/// <summary>
		/// Constructor for the Items window class
		/// </summary>
		public Items() {
			try {
				InitializeComponent();

				selectedRow = 0; //set the highlight to the first item in the list
				loadGrid(); //load the data grid


				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				} //end catch
			} //end constructor



		/// <summary>
		/// Handles user clicking the Close button
		/// </summary>
		private void btnClose_Click(object sender, RoutedEventArgs e) {
			try {
				this.Hide();
				} //end try
			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			}


		/// <summary>
		/// Handles user clicking the X button
		/// </summary>
		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			try {
				e.Cancel = true;
				this.Hide();
				} //end try
			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			}
			 

		/// <summary>
		/// Handles an error
		/// </summary>
		/// <param name="sClass">The class in which the error occurred</param>
		/// <param name="sMethod">The method in which the error occurred</param>
		private void HandleError(string sClass, string sMethod, string sMessage) {
			try {
				MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
				} //end try

			catch (Exception ex) {
				System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
											 "HandleError Exception: " + ex.Message);
				}
			}


		/// <summary>
		/// Handles the user clicking the Add button
		/// Opens a new instance of the NewItem window
		/// </summary>
		private void btnAdd_Click(object sender, RoutedEventArgs e) {
			myNewItem = new NewItem(); //initialize the New Item window
			myNewItem.itemsRef = this; //give the New Items window a reference back to this window
			myNewItem.ShowDialog(); //Show the window
			this.Show(); //Show the main form after the user closes the NewItem window
			}

		/// <summary>
		/// Handles the user clicking the Delete button
		/// </summary>
		private void btnDelete_Click(object sender, RoutedEventArgs e) {
			try {
				if (myItemSet.HasInvoices(Convert.ToInt32(tbID.Text))) { //if there are invoices linked to the selected item
					StringBuilder sb = new StringBuilder(); //build a string warning the user
					sb.Append("The item cannot be deleted because it is linked to the following invoices:");
					sb.Append('\r');
					foreach (Invoice i in myItemSet.myInvoices) { //add the ID of each linked invoice
						sb.Append('\t');
						sb.Append(i.InvoiceNum.ToString());
						sb.Append('\r');
						} //end foreach
					MessageBox.Show(sb.ToString(), "Cannot Delete Item"); //display the warning msg. & linked invoice numbers
					} //end if

				else { //if there are not any linked invoices
					StringBuilder sb = new StringBuilder(); //build a string to get final confirmation
					sb.Append("Are you sure you wish to delete this item?");
					sb.Append('\r');
					sb.Append('\r');
					sb.Append('\t');
					sb.Append("ID: ");
					sb.Append(tbID.Text);
					sb.Append('\r');
					sb.Append('\t');
					sb.Append("Name: ");
					sb.Append(tbName.Text);
					sb.Append('\r');
					sb.Append('\t');
					sb.Append("Description: ");
					sb.Append(tbDesc.Text);
					sb.Append('\r');
					sb.Append('\r');
					sb.Append('\r');
					sb.Append("Click Yes to Delete the item. Click No to cancel.");
					sb.Append('\r');

					//if the user presses the final "Yes" to delete the item
					if (MessageBox.Show(sb.ToString(), "Confirm Delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
						myItemSet.DeleteCurrentItem(Convert.ToInt32(tbID.Text)); //really delete it
						loadGrid(); //reload the grid
						lblChanges.Content = "The selected item has been deleted"; //update user message

						} //end if

					} //end else
				} //end try
			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			} //end btnDelete_Click


		/// <summary>
		/// Handles the event that occurs when the user clicks on a different row in the data grid
		/// </summary>
		private void dataGridItems_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			try {
				resetFields(dataGridItems.SelectedIndex); //updates the fields at the bottom of the form
				lblChanges.Content = "Make desired change and click Save Changes"; //resets the msg. about changing the item
				} //end try
			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			}


		/// <summary>
		/// Handles the event that occurs when the data grid is initially loaded
		/// </summary>
		private void dataGridItems_Loaded(object sender, RoutedEventArgs e) {
			try {
				if (selectedRow != 0) {	//if the grid is being reloaded after a change
					dataGridItems.SelectedIndex = selectedRow; //reset the highlight to the row that was just changed
					} //end if
				else {
					dataGridItems.SelectedIndex = 0; //automatically focus on row 1 when loading for the first time
					} //end else
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			}


		/// <summary>
		/// Handles the event that occurs when the user clicks the Save button
		/// </summary>
		private void btnSave_Click(object sender, RoutedEventArgs e) {
			try {
				String message = ""; //a variable to hold a message back to the user

				if (myItemSet.ValidateChange(ref message, tbID.Text,
						tbName.Text, tbDesc.Text, tbPrice.Text) == true) {//see if the change is valid

						selectedRow = dataGridItems.SelectedIndex; //save the selected row so it can be highlighted later
						myItemSet.ItemChanged(); //make the change
						loadGrid(); //reload the grid
						lblChanges.Content = message; //set the status message
					} //end if

				else { //if the change is not valid
					lblChanges.Content = message; //set the status message
					resetFields(Convert.ToInt32(tbID.Text)); //reset the fields (remove the change the user entered)
					} //end else
				} //end try
			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			} //end btnSaveClick


		/// <summary>
		/// Handles the event that occurs when the user clicks the Cancel button
		/// </summary>
		private void btnCancel_Click(object sender, RoutedEventArgs e) {
			try {
				resetFields(dataGridItems.SelectedIndex); //reset the fields at the bottom of the form to the database values
				lblChanges.Content = "Make desired change and click Save Changes"; //display a message to the user
				} //end try
			catch (Exception ex) {
				//This is the top level method so we want to handle the exception
				HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
							MethodInfo.GetCurrentMethod().Name, ex.Message);
				}
			} //end btnCancel_click

		/// <summary>
		/// Load the fields at the bottom of the form from the DataGrid
		/// </summary>
		/// <param name="i">Int representing the index of the currently highlighted row in the grid (0 indexed)</param>
		public void resetFields(int i) {
			try {
				dataGridItems.SelectedIndex = i; //set the index to the passed-in value

				if (dataGridItems.SelectedIndex == -1) { //if loading after a change
					dataGridItems.SelectedIndex = selectedRow; // highlight row that was changed
					} 

				Item selectedItem = (dataGridItems.SelectedItem as Item); //store the string

                tbID.Text = selectedItem.ItemID.ToString(); //ID
                tbName.Text = selectedItem.ItemName.ToString(); //Name
                tbDesc.Text = selectedItem.ItemDescription.ToString(); //Desc
                tbPrice.Text = selectedItem.ItemCost.ToString(); //Price
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end resetFields


		/// <summary>
		/// Load the data grid with data from the database
		/// </summary>
		public void loadGrid() {
			try {
				myItemSet = new ItemSet(); //initialize the ItemSet object

				//populate the DataGrid
				dataGridItems.ItemsSource = myItemSet.myList; //the "myList" List in ItemSet contains the list of Items
				dataGridItems.SelectedIndex = selectedRow; //highlight the row that was just changed
				} //end try
			catch (Exception ex) {
				//Just throw the exception
				throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
									MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
				}
			} //end loadGrid
		#endregion


		} //end class
	} //end namespace
