﻿

//CS 3280 Spring 2014
//Group 3 Final Project
//Jenkins, Nicole   nicole.defriez@gmail.com 
//Thurston, Zachary	  zach@zthurston.com 
//Wenzbauer, Jeffrey   jeffwenzbauer@mail.weber.edu

//Due Date: April 16



/*REQUIREMENTS:

Create a Windows WPF program that can be used as in invoice system for a small business.  The type of business is up to you.  
Examples of a business would be a Supplement Store, Jewelry Store, Shoe Store, Equipment Rental Store, etc.  


MAIN (INVOICE) WINDOW
------------------
--The main form should allow the user to create new invoices, edit existing invoices, or delete existing invoices.  
--It will also have a menu that will have two functionalities.  
--The first will allow the user to update a def table that contains the items.  (*I renamed this to "Items" --Nicole)
--The next will be to open a search screen used to search for invoices.
--If a new invoice is created the user may enter data pertaining to that invoice.  
--An auto-generated number from the database will be given to the invoice as the invoice number.  
--An invoice date will also be assigned by the user.  
--Next different items will be entered by the user.  
--The items will be selected from a drop down box and the cost for that item will be put into a read only textbox.  
--This will be the default cost of an item. Once the item is selected, the user can add the item.  
--As many items as needed should be able to be added.  
--All items entered should be displayed for viewing in a list (something like a DataGridView).  
--Items may be deleted from the list.  
--A running total of the cost should be displayed as items are entered or deleted.
--Once all the items are entered the user can save the invoice.  
--This will lock the data in the invoice for viewing only.  
--From here the user may choose to Edit the Invoice or Delete the Invoice.


INVOICE SEARCH
------------------
--The user also needs to be able to search for invoices, which will be a choice from the menu.  
--On the search screen all invoices should be displayed in a list (like a DataGridView) for the user to select.  
--The user may limit the invoices displayed by choosing an Invoice Number from a drop down, selecting an invoice date, or selecting the total charge from a drop down box.  
--When a limiting item is selected the list should only reflect those invoices that match the criteria.  
--Once an invoice is selected the user will click a “Select Invoice” button, which will close the search form and open the selected invoice up for viewing on the main screen.  
--From there the user may choose to Edit or Delete the invoice.


"ITEMS" (**I renamed this to ITEMS everywhere because 'def' wasn't very descriptive)
------------------
--The last form needed is a form to update the def ("Items") table which contains all the items for the business.  
--This form can be accessed through the menu and only when an invoice is not being edited or a new invoice is being entered.  
--This form will list all the items in a list (like a DataGridView).  
-- The items will consist of a name, cost, and description.  
--From here the user can add new items, edit existing items, or delete existing items.  
--If the user tries to delete an item that is on an invoice, don’t allow the user to do so.  
--Instead warn them with a message that tells the user which invoices that item is used on.  
--When the user closes the update def ("Items") table form, make sure to update the drop down box as to reflect any changes made by the user.
--Also update the current invoice because its item name might have been updated.


GENERAL
------------------
--Since this is the final project all lessons learned throughout the course should be used and implemented.  
--A Microsoft Access database should be used as the backend database to store the invoice data.
--Don’t forget to abstract your business logic into classes and keep you UI code clean.  
--Make sure to test all user inputs so your program doesn’t crash, have another group member test your code thoroughly.  
--All methods should handle exceptions.  
--Since this a WPF application you should use styles for your applications.  
--Visual properties shouldn’t be hard coded into controls, they should be put into styles and applied to controls.

 
 */




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Reflection;
using System.Windows.Shapes;
using System.Data;
using System.ComponentModel;

namespace Group3FinalProject
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Attributes

        /// <summary>
        /// Items window
        /// </summary>
        Items myItemsWindow;

        /// <summary>
        /// Invoice Search window
        /// </summary>
        InvoiceSearch myInvoiceSearch;

        /// <summary>
        /// The invoice that is being displayed on the screen.
        /// </summary>
        Invoice selectedInvoice;

        /// <summary>
        /// The invoice that will be deleted from the database when the delete button is pressed
        /// </summary>
        Invoice invoiceToBeDeleted;

        /// <summary>
        /// Contains the items that are displayed in the combobox on the GUI
        /// </summary>
        ItemSet myItemSet;

        /// <summary>
        /// Will be set to true when the user clicks the "New Invoice" button.
        /// Used as a trigger in the handling of the events following the selection of a date.
        /// </summary>
        bool creatingInvoice;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor of the MainWindow
        /// </summary>
        public MainWindow()
        {
            try
            {

                InitializeComponent();

                //MAKE SURE TO INCLUDE THIS LINE OR THE APPLICATION WILL NOT CLOSE
                //BECAUSE THE WINDOWS ARE STILL IN MEMORY
                Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;

                //Initialize objects
                myItemsWindow = new Items();
                myInvoiceSearch = new InvoiceSearch();
                selectedInvoice = new Invoice();

                populateItemsCombobox();
            } //end try
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }
        #endregion

        #region GUI Refreshing Methods

        /// <summary>
        /// Gets the items that are contained in the database and loads them into the combobox on the gui
        /// </summary>
        private void populateItemsCombobox()
        {
            try
            {
                // Update the itemset
                myItemSet = new ItemSet();

                // Bind to the ItemSet
                cboItems.ItemsSource = myItemSet.myList;
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Loads all data from the invoice stored in the class level variable "selectedInvoice" into the fields on the window.
        /// </summary>
        private void loadInvoiceData(bool invoiceDeleted = false)
        {
            try
            {
                if (!invoiceDeleted)
                {
                    // Refresh the items in the invoice
                    selectedInvoice.getInvoiceItems();

                    //Bind the DataTable of invoice items to the data grid
                    dgInvoiceItems.ItemsSource = selectedInvoice.myItems;

                    // If the invoice number exists
                    if (selectedInvoice.InvoiceNum != -1)
                    {
                        // Display the invoice number
                        lblInvoiceNumber.Content = selectedInvoice.InvoiceNum.ToString();
                    }
                    else
                    {
                        // Display nothing in the invoice number label
                        lblInvoiceNumber.Content = "";
                    }


                    // If the invoice date is not yet set
                    if (selectedInvoice.InvoiceDate == new DateTime())
                    {
                        // Display "select a date" in the datepicker
                        dateInvoiceDate.SelectedDate = null;
                    }
                    // If the invoice date has been set
                    else
                    {
                        // Display the invoice date
                        dateInvoiceDate.SelectedDate = selectedInvoice.InvoiceDate;
                    }

                    // If there is a total charge
                    if (selectedInvoice.TotalCharge != -1)
                    {
                        // Display the invoice Total Price
                        lblTotalPriceAmount.Content = "$" + selectedInvoice.TotalCharge.ToString();
                    }
                    else
                    {
                        // Display nothing in the total price label
                        lblTotalPriceAmount.Content = "";
                    }
                }
                // If an invoice was just deleted
                else
                {
                    // Display nothing in the invoice number label
                    lblInvoiceNumber.Content = "";

                    // Display "select a date" in the datepicker
                    dateInvoiceDate.SelectedDate = null;

                    // Display nothing in the total price label
                    lblTotalPriceAmount.Content = "";

                    // Clear the selected invoice
                    selectedInvoice = new Invoice();

                    //Bind the DataTable of invoice items to the data grid
                    dgInvoiceItems.ItemsSource = selectedInvoice.myItems;
                }
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        } // end loadInvoiceData

        /// <summary>
        /// This is used to update the items contained in the datagrid after an add item or delete item 
        /// has occurred.  This function is used instead of the LoadInvoiceData function because the add/delete
        /// has not yet been committed to the database.  The update of data into the datagrid occurs simply out
        /// the list object contained in the invoice.
        /// </summary>
        private void preSaveDataGridRefresh()
        {
            // Display the invoice Total Price
            lblTotalPriceAmount.Content = "$" + selectedInvoice.TotalCharge.ToString();

            // Create a temp invoice with a blank list
            Invoice temp = new Invoice();

            // Bind the datagrid to the blank list (this gets rid of inconsistencies that occur
            // when calling the .Remove() function on the list of Items that is bound to the
            // datagrid.
            dgInvoiceItems.ItemsSource = temp.myItems;

            // Bind the datagrid to the populated list
            dgInvoiceItems.ItemsSource = selectedInvoice.myItems;

            // Refresh the GUI
            dgInvoiceItems.Items.Refresh();
        }

        /// <summary>
        /// Enables items on the GUI enabled to allow the user to edit the invoice
        /// </summary>
        private void makeInvoiceEditableGUI()
        {
            btnNewInvoice.IsEnabled = false;
            cboItems.IsEnabled = true;
            btnAddItem.IsEnabled = true;
            btnSaveInvoice.IsEnabled = true;
            btnDeleteItem.IsEnabled = true;
            btnEditInvoice.IsEnabled = false;
            dateInvoiceDate.IsEnabled = false;
            cbMainMenu.IsEnabled = false;
        }

        /// <summary>
        /// Disables items on the GUI so that the invoice is not editable by the user
        /// </summary>
        private void makeInvoiceSavedGUI()
        {
            btnNewInvoice.IsEnabled = true;
            cboItems.IsEnabled = false;
            btnAddItem.IsEnabled = false;
            btnSaveInvoice.IsEnabled = false;
            btnDeleteItem.IsEnabled = false;
            dateInvoiceDate.IsEnabled = false;
            btnEditInvoice.IsEnabled = true;
            btnDeleteInvoice.IsEnabled = true;
            cbMainMenu.IsEnabled = true;

            // Hide the Item price
            lblItemCostHeader.Visibility = Visibility.Hidden;
            lblItemCostAmount.Visibility = Visibility.Hidden;

            // Clear the selected item in the items combobox
            cboItems.SelectedIndex = -1;

            // Refresh the Invoice Search window
            myInvoiceSearch.RefreshInvoiceUI();
        }

        #endregion

        #region Error Handler

        /// <summary>
        /// Handles an error
        /// </summary>
        /// <param name="sClass">The class in which the error occurred</param>
        /// <param name="sMethod">The method in which the error occurred</param>
        private void HandleError(string sClass, string sMethod, string sMessage)
        {
            try
            {
                MessageBox.Show(sClass + "." + sMethod + " -> " + sMessage);
            } //end try

            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Error.txt", Environment.NewLine +
                                             "HandleError Exception: " + ex.Message);
            }
        }

        #endregion

        #region BackgroundWorker Methods

        /// <summary>
        /// Updates the total cost of the invoice that is passed into the backgroundworker in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCostWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string[] myData = (string[])e.Argument;
                string tempPrice = myData[0];
                string tempInvoiceNum = myData[1];

                // If the invoice does not have any items added to it yet
                if (tempPrice == "")
                {
                    tempPrice = "0";
                }
                else
                {
                    tempPrice = tempPrice.Substring(1);
                }

                Invoice.updateTotalCharge(tempPrice, tempInvoiceNum);
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Deletes the seleectedInvoice from the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteInvoicesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // Delete the invoice from the database
                invoiceToBeDeleted.deleteInvoice();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Refreshes the GUI elements in the Invoice Search window to get rid of all information
        /// related to the invoice that was just deleted using the BackgroundWorker.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteInvoicesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                // Refresh the Invoice Search window
                myInvoiceSearch.RefreshInvoiceUI();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Updates the ui in the invoice search form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCostWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                // Disable the buttons and elements on the GUI so that the invoice is not editable
                makeInvoiceSavedGUI();
            }
            catch (Exception ex)
            {
                //Just throw the exception
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." +
                                    MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion

        #region SelectionChanged Methods

        //  ******************************************************************************
        //  I did this combo box to provide temporary navigation through the forms
        //	This control needs to be changed to something that works better as a menu
        // -Nicole
        // *****************************************************************************
        /// <summary>
        /// Handle click of the MainMenu combo box
        /// </summary>
        private void cbMainMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbMainMenu.SelectedIndex == 0)
                { //if the user selected Item Definitions
                    this.Hide(); //Hide the menu
                    myItemsWindow.ShowDialog(); //Show the window
                    this.Show(); //Show the main form

                    loadInvoiceData(true);

                    // Refresh the items combobox
                    populateItemsCombobox();

                    // reset the combobox
                    cbMainMenu.SelectedIndex = -1;
                } //end if
                else if (cbMainMenu.SelectedIndex == 1)
                { //if the user selected InvoiceSearch
                    this.Hide(); //Hide the menu
                    myInvoiceSearch.ShowDialog(); //Show the window
                    this.Show(); //Show the main form

                    // reset the combobox
                    cbMainMenu.SelectedIndex = -1;

                    // If an invoice was selected for viewing by the user in the search window
                    if (myInvoiceSearch.InvoiceSelected)
                    {
                        // Store the invoice that was selected by the user in the search window into the
                        // local "selectedInvoice" object.
                        selectedInvoice = myInvoiceSearch.SelectedInvoice;

                        // Reset the selected invoice data in the invoice search window
                        myInvoiceSearch.SelectedInvoice = new Invoice();
                        myInvoiceSearch.InvoiceSelected = false;

                        // Load the selected invoice data into the window
                        loadInvoiceData();

                        // Enable the edit and delete buttons
                        btnDeleteInvoice.IsEnabled = true;
                        btnEditInvoice.IsEnabled = true;

                        // Disable the date picker
                        dateInvoiceDate.IsEnabled = false;
                    }
                } //end else			
            } //end Try
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        } //end method

        /// <summary>
        /// Handles the functionality behind the selecting of a date in the datepicker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateInvoiceDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is DatePicker)
            {
                // If a valid date is selected in the date picker on the GUI
                if (((DatePicker)sender).SelectedDate != null && creatingInvoice)
                {
                    //Create a new invoice object
                    selectedInvoice = new Invoice();

                    // bind the datagrid to the new invoice
                    dgInvoiceItems.ItemsSource = selectedInvoice.myItems;

                    // get the date that was selected by the user for the invoice
                    selectedInvoice.InvoiceDate = Convert.ToDateTime(((DatePicker)sender).SelectedDate);

                    // Create a new invoice in the database and set the invoice number label equal to the new invoice number
                    lblInvoiceNumber.Content = selectedInvoice.createInvoice(dateInvoiceDate.SelectedDate.ToString());

                    // Clear the price on the screen from the previously displayed invoice
                    lblTotalPriceAmount.Content = "";

                    //Enable all the appropriate controls on the GUI
                    btnDeleteInvoice.IsEnabled = true;
                    btnSaveInvoice.IsEnabled = true;
                    cboItems.IsEnabled = true;
                    btnAddItem.IsEnabled = true;
                    btnNewInvoice.IsEnabled = false;
                    btnDeleteItem.IsEnabled = true;
                    btnEditInvoice.IsEnabled = false;
                    txtNewInvoiceInstructions.Visibility = Visibility.Hidden;
                    imgDateArrow.Visibility = Visibility.Hidden;
                    dateInvoiceDate.IsEnabled = false;

                    // reset the trigger
                    creatingInvoice = false;
                }
            }

        }

        /// <summary>
        /// Handles the functionality behind the selecting of an item in the combobox containing items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (sender is ComboBox)
                {
                    if ((sender as ComboBox).SelectedIndex != -1)
                    {
                        // Un Hide the item cost on the GUI
                        lblItemCostHeader.Visibility = Visibility.Visible;
                        lblItemCostAmount.Visibility = Visibility.Visible;

                        if ((sender as ComboBox).SelectedItem is Item)
                        {
                            lblItemCostAmount.Content = "$" + ((sender as ComboBox).SelectedItem as Item).ItemCost.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        #endregion

        #region ButtonClick Methods

        /// <summary>
        /// Handles functionality behind the Close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Handles Functionality behind the New Invoice Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNewInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Clear the datepicker
                dateInvoiceDate.SelectedDate = null;

                cbMainMenu.IsEnabled = false;

                // Used as a trigger in the dateInvoiceDate_SelectedDateChanged function
                creatingInvoice = true;

                // Enable the Datepicker and wait for the user to select a date before creating an invoice
                dateInvoiceDate.IsEnabled = true;
                txtNewInvoiceInstructions.Visibility = Visibility.Visible;
                imgDateArrow.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Handles functionality behind the Edit Invoice button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEditInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Enable the buttons and elements on the GUI
                makeInvoiceEditableGUI();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Handles functionality behind the Save Invoice button, including committing all 
        /// deleting/adding of items into the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // If the label does not contain a price
                if (string.IsNullOrEmpty(lblTotalPriceAmount.Content.ToString()))
                {
                    // Update the totalCharge in the invoice
                    selectedInvoice.TotalCharge = 0;
                }
                else
                {
                    // Update the totalCharge in the invoice
                    selectedInvoice.TotalCharge = Convert.ToDecimal(lblTotalPriceAmount.Content.ToString().Substring(1));
                }
                

                // Store the invoice number and the total price into a string array to be passed into the background worker
                string[] backgroundWorkerData = new string[2];
                backgroundWorkerData[0] = lblTotalPriceAmount.Content.ToString();
                backgroundWorkerData[1] = lblInvoiceNumber.Content.ToString();

                // Use a background worker to update the total cost of the invoice in the database
                BackgroundWorker UpdateCostWorker = new BackgroundWorker();
                UpdateCostWorker.DoWork += new DoWorkEventHandler(UpdateCostWorker_DoWork);
                UpdateCostWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(UpdateCostWorker_RunWorkerCompleted);
                UpdateCostWorker.RunWorkerAsync(backgroundWorkerData);
                
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Handles functionality behind the Delete Invoice button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // store the current invoice into the invoice instance to be deleted
                invoiceToBeDeleted = selectedInvoice;

                // data on the GUI related to the invoice that was just deleted
                loadInvoiceData(true);

                // Lock the the unneeded buttons
                btnDeleteInvoice.IsEnabled = false;
                btnEditInvoice.IsEnabled = false;
                btnSaveInvoice.IsEnabled = false;
                btnDeleteItem.IsEnabled = false;
                cboItems.IsEnabled = false;
                btnAddItem.IsEnabled = false;
                cbMainMenu.IsEnabled = true;
                btnNewInvoice.IsEnabled = true;
                dateInvoiceDate.IsEnabled = false;

                // Use a backgroundworker to delete the invoice from the database and refreh the datagrid
                // contained in the invoice search window so that the UI does not lag while these
                // operations are performed.
                BackgroundWorker DeleteInvoicesWorker = new BackgroundWorker();
                DeleteInvoicesWorker.DoWork += new DoWorkEventHandler(DeleteInvoicesWorker_DoWork);
                DeleteInvoicesWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(DeleteInvoicesWorker_RunWorkerCompleted);
                DeleteInvoicesWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Handles functionality behind the Add Item button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // If there is a selected item
                if (cboItems.SelectedIndex != -1)
                {
                    // Add the item selected in the combobox to the invoice in the database
                    selectedInvoice.addItem((cboItems.SelectedItem as Item));

                    // Refresh the GUI withough requesting data from the database
                    preSaveDataGridRefresh();

                    // Refresh the GUI through requesting data from the database

                    txtAddItemError.Visibility = Visibility.Hidden;
                }
                else
                {
                    // Display a message to tell the user they need to select an item from the combobox
                    txtAddItemError.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        /// <summary>
        /// Handles functionality behind the Delete Item Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // If there is a selected item in the datagrid
                if (dgInvoiceItems.SelectedItems.Count != 0)
                {
                    // if the selected item in the datagrid is an Item class
                    if (dgInvoiceItems.SelectedItems[0] is Item)
                    {
                        // Get the selected item from the datagrid
                        Item tempItem = (dgInvoiceItems.SelectedItems[0] as Item);

                        // Delete the item from the list of items contained in the invoice
                        selectedInvoice.deleteItem(tempItem);

                        // Refresh the GUI withough requesting data from the database TODO: get the multithread working then use this function
                        //preSaveDataGridRefresh();

                        // Refresh the GUI through requesting data from the database
                        loadInvoiceData();

                        // Display a message to tell the user they need to select an item within the datagrid to delete
                        txtDeleteItemError.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    // Display a message to tell the user they need to select an item within the datagrid to delete
                    txtDeleteItemError.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                //This is the top level method so we want to handle the exception
                HandleError(MethodInfo.GetCurrentMethod().DeclaringType.Name,
                            MethodInfo.GetCurrentMethod().Name, ex.Message);
            }
        }

        #endregion

    } //end class
} //end namespace
